import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LinearProgress from '@material-ui/core/LinearProgress';
import './index.css'

const BorderLinearProgress = withStyles((theme) => ({
    root: {
        height: 10,
        borderRadius: 5,
        width: '80%',
        margin: '3% 3%'
    },
    colorPrimary: {
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
        borderRadius: 5,
        backgroundColor: 'orange'

    },
}))(LinearProgress);

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        borderRadius: '2%',
        // marginLeft: '5%',
        marginTop: '5%',

    },
    MuiAccordionDetailsRoot :{
        height:'60px'
      },
    matchsTopics: {
        // height: '115px',
        marginBottom: '5%',
        backgroundColor: '#f2f2f2',
        borderRadius: '12px !important',
        boxShadow: '1px 1px 1px 1px darkgrey'


    },
    heading: {
        fontWeight: 'bold',
        fontSize: '18px'
    },
    subjectHeader: {
        height: '120px',
        width: '100%',
        backgroundColor: '#aec2ea',
        borderRadius: '6px',
        paddingLeft: '2px',
        marginBottom: '4%',

    },
    mathsHeadingOne: {
        fontWeight: 'bold',
        fontSize: '20px'
    },
    topics: {
        float: 'right'
    }
}));

export default function AllSubjects(valueTest) {
    const classes = useStyles();
    // console.log('valesCheck-->', this.props.abc);
    const test = valueTest;
    console.log('valueTest at Maths-->', test);

    return (
        <React.Fragment>
            <div className='parentDiv'>
            <div className={classes.root}>
            <div className={classes.subjectHeader}>
                <span className={classes.mathsHeadingOne}>Maths(27Q)</span> <span className={classes.topics}>3 Topics</span>
                <BorderLinearProgress variant="determinate" value={35} />
            </div>
            <Accordion className={classes.matchsTopics}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <Typography className={classes.heading}>Pure Maths(10Q)
            <BorderLinearProgress className={classes.subBar} variant="determinate" value={100} />
                    </Typography>
                </AccordionSummary>
                    <BorderLinearProgress variant="determinate" value={50} style={{ height: '5px'}}/>

                    <BorderLinearProgress variant="determinate" value={80} />
            </Accordion>
            <Accordion className={classes.matchsTopics}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}>Calculas(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />

                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Progress bar follows
          </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion className={classes.matchsTopics}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                >
                    <Typography className={classes.heading}>Binominal Theorem(10Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />

                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Progress bar follows
          </Typography>
                </AccordionDetails>
            </Accordion>
        </div>
            </div>
            <div className='parentDiv'>
            <div className={classes.root}>
      <div className={classes.subjectHeader}>
        <h2>Physics(29Q)</h2> <span>3 Topics</span>
        <BorderLinearProgress variant="determinate" value={40} />
      </div>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>General Physics(5Q)
        <BorderLinearProgress variant="determinate" value={45} />

          </Typography><br />
        </AccordionSummary>
        <AccordionDetails>
            <BorderLinearProgress variant="determinate" value={10} />
            <BorderLinearProgress variant="determinate" value={16} />
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>Heat & Thermodynamics(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Progress bar follows
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>Moderan Physics(12Q)
          <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Progress bar follows
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
            </div>
            <div className='parentDiv'>
            <div className={classes.root}>
      <div className={classes.subjectHeader}>
        <h2>Chemistry(29Q)</h2> <span>3 Topics</span>
        <BorderLinearProgress variant="determinate" value={35} />

      </div>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>Organic Chemistry(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Progress bar follows
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>Inorganic Chemistry(5Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />

          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Progress bar follows
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.matchsTopics}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2a-content"
          id="panel2a-header"
        >
          <Typography className={classes.heading}>Physical Chemistry(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />

          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Progress bar follows
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
            </div>
        </React.Fragment>
    );
}
