import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import LinearProgress from '@material-ui/core/LinearProgress';

import './index.css'

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
    width: '80%',
    margin: '3% 3%'
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: 'orange'
  }

}))(LinearProgress);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    borderRadius: '2%',
  },
  matchsTopics: {
    height: '115px',
    marginBottom: '5%',
    backgroundColor: '#f2f2f2',
    borderRadius: '12px !important',
    boxShadow: '1px 1px 1px 1px darkgrey',
    marginRight: '5%'

  },
  heading: {
    fontWeight: 'bold',
    fontSize: '18px'
  },
  subBar: {
    width: '100%',
  },
  subjectHeader: {
    height: '120px',
    width: '64%',
    backgroundColor: '#aec2ea',
    borderRadius: '6px',
    paddingLeft: '2px',
    marginBottom: '2%',
  },
  mathsHeadingOne:{
    fontWeight:'bold',
    fontSize:'20px'
},
topics:{
    float:'right'
}
}));
export default function PhysicsSession(valueTest) {
  const test = valueTest;
  console.log('valueTest at Physics-->', test);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.subjectHeader}>
        <span className={classes.mathsHeadingOne}>Physics(29Q)</span>
                <span className={classes.topics}>3 Topics</span>
        <BorderLinearProgress variant="determinate" value={40} />
      </div>
      <div style={{ float: 'left', width: '33%' }}>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>General Physics(5Q)
        <BorderLinearProgress variant="determinate" value={45} />

            </Typography><br />
          </AccordionSummary>
          <AccordionDetails>
            <BorderLinearProgress variant="determinate" value={10} />
            <BorderLinearProgress variant="determinate" value={16} />
          </AccordionDetails>
        </Accordion>

        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Heat & Thermodynamics(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Moderan Physics(12Q)
          <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
      <div style={{ float: 'left', width: '33%' }}>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>General Physics(5Q)
        <BorderLinearProgress variant="determinate" value={45} />

            </Typography><br />
          </AccordionSummary>
          <AccordionDetails>
            <BorderLinearProgress variant="determinate" value={10} />
            <BorderLinearProgress variant="determinate" value={16} />
          </AccordionDetails>
        </Accordion>

        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Heat & Thermodynamics(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Moderan Physics(12Q)
          <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
      <div style={{ float: 'left', width: '33%' }}>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography className={classes.heading}>General Physics(5Q)
        <BorderLinearProgress variant="determinate" value={45} />

            </Typography><br />
          </AccordionSummary>
          <AccordionDetails>
            <BorderLinearProgress variant="determinate" value={10} />
            <BorderLinearProgress variant="determinate" value={16} />
          </AccordionDetails>
        </Accordion>

        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Heat & Thermodynamics(12Q)
        <BorderLinearProgress className={classes.subBar} variant="determinate" value={65} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion className={classes.matchsTopics}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>Moderan Physics(12Q)
          <BorderLinearProgress className={classes.subBar} variant="determinate" value={20} />
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              Progress bar follows
          </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
    </div>
  );
}