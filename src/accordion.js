

import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MathematicSession from './MathematicSession';
import PhysicsSession from './PhysicsSession';
import ChemicalSession from './ChemicalSession'
import AllSubjects from './AllSubjects'
import './index.css'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '80%',
    margin: '1% 10%'
  }

}));

export default function SimpleAccordion() {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);
  // const [subjectvalue, setPlayers] = useState([
  //   {
  //     value: "0"
  //   }
  // ]);


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="white">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
          style={{ float: "right" }}
        >
          <Tab label="All" {...a11yProps(0)} style={{ minWidth: "1%", maxWidth: '5%', marginRight: '2%' }} />
          <Tab label="Maths" {...a11yProps(1)} style={{ minWidth: "1%", maxWidth: '5%', marginRight: '2%' }} />
          <Tab label="Physics" {...a11yProps(2)} style={{ minWidth: "1%", maxWidth: '8%', marginRight: '2%' }} />
          <Tab label="Chemistry" {...a11yProps(3)} style={{ minWidth: "1%", maxWidth: '9%', marginRight: '2%' }} />

        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
        style={{ width: "103%" }}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <AllSubjects></AllSubjects>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
            <MathematicSession valueTest={value} ></MathematicSession>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
            <PhysicsSession valueTest={value}></PhysicsSession>
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}>
            <ChemicalSession valueTest={value}></ChemicalSession>
        </TabPanel>
      </SwipeableViews>
    </div>
  );
}





// import React from 'react';



// export default function SimpleAccordion() {
//   return (
//     <div >
//       <div className='headerDiv'>
//         <div className='headerSub'> 12 CBSE</div>
//         <div className='headerOpt'>
//           <a href='#' className='headerOptNames'>All</a>
//           <a href='#' className='headerOptNames'>Maths</a>
//           <a href='#' className='headerOptNames'>Physics</a>
//           <a href='#' className='headerOptNames'>Chemistry</a>
//         </div>
//       </div>

//     </div>
//   );
// }




